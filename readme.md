# TV Show Explorer

## Install dependencies

`yarn // or npm install`

## Start App locally

`yarn start // or npm start`

## Bundle App

`yarn build // or npm run build`

You'll find files in `/dist` folder
