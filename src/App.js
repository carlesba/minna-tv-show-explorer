import React from 'react'
import SearchBox from './components/SearchBox'
import createFetchShows from './remote/fetchShows'
import Loading from './components/Loading'
import Message from './components/Message'
import Shows from './components/Shows'
import './reset.css'
import './App.css'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.update = this.update.bind(this)
    const state = this.props.Store().value
    this.state = state
    this.fetch = createFetchShows(window.fetch)
  }
  componentDidMount() {
    const { value, searchShow } = this.getStore()
    searchShow(value.query)
  }
  getStore() {
    return this.props.Store(this.state, this.update, this.fetch)
  }
  update(state) {
    console.log('update >>', state)
    this.setState(state)
  }
  render() {
    const { value, updateQuery, searchShow } = this.getStore()

    return (
      <div className="app">
        <SearchBox
          value={value.query}
          onChange={updateQuery}
          onSubmit={searchShow}
        />
        {value.status === 'PENDING' && <Loading />}
        {value.status === 'FAILURE' && <Message value={value.reason} />}
        {value.status === 'SUCCESS' && <Shows value={value.data} />}
      </div>
    )
  }
}

export default App
