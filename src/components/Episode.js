import React from 'react'
import './Episode.css'
import createFetchAPI from '../remote/fetchAPI'

const fetchAPI = createFetchAPI(fetch)

class Episode extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.imdbID,
      number: props.Episode,
      title: props.Title,
      rating: props.imdbRating,
      status: 'IDLE'
    }
  }
  componentDidMount() {
    fetchAPI(this.state.id)
      .then(data => {
        this.setState({
          status: 'SUCCESS',
          plot: data.Plot,
          poster: data.Poster,
          monthReleased: data.Released.split(' ')[1]
        })
      })
      .catch(err => {
        this.setState({
          status: 'FAILURE',
          reason: err.message
        })
      })
  }
  render() {
    const {
      title,
      rating,
      monthReleased,
      reason,
      status,
      number,
      plot,
      poster
    } = this.state
    return (
      <div className="episode">
        <header>
          <h1 className="episode_title">{number + '. ' + title}</h1>
          <div>
            <span className="episode_rating">{rating + '★'}</span>
            <span className="episode_released">
              released in {monthReleased}
            </span>
          </div>
        </header>
        <div className="episode_summary">
          <img src={poster} className="episode_poster" />
          <p className="episode_plot">{plot}</p>
        </div>
      </div>
    )
  }
}

export default Episode
