import React from 'react'
import Episode from './Episode'
import './Shows.css'

const Shows = ({ value }) => (
  <div className="shows">
    {value.Episodes.map(episode => (
      <Episode key={episode.imdbID} {...episode} />
    ))}
  </div>
)

export default Shows
