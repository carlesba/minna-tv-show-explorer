import React from 'react'
import './SearchBox.css'

class SearchBox extends React.Component {
  componentDidMount() {
    this.refs.input.focus()
  }
  render() {
    const { value, onChange, onSubmit } = this.props

    function handleSubmission(event) {
      event.preventDefault()
      onSubmit(value)
    }
    function handleInputChange(event) {
      onChange(event.target.value)
    }

    return (
      <form className="search-box" onSubmit={handleSubmission}>
        <input
          className="search-box_input"
          ref="input"
          type="text"
          value={value}
          onChange={handleInputChange}
        />
        <button className="search-box_button" type="submit">
          Search
        </button>
      </form>
    )
  }
}

export default SearchBox
