import React from 'react'
import './Message.css'

const Message = ({ value }) => <div className="message">{value}</div>

export default Message
