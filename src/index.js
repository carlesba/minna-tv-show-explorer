import React from 'react'
import DOM from 'react-dom'
import App from './App'
import Store from './Store'

function mount(domNode) {
  DOM.render(<App Store={Store} />, domNode)
}

if (typeof window === 'object') {
  window.mount = mount
}
