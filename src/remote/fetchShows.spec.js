import createFetchShows from './fetchShows'

test('Appends API_KEY and type to query', () => {
  const mockFetch = jest.fn(() =>
    Promise.resolve({
      json() {
        return 'data'
      }
    })
  )
  const fetchShows = createFetchShows(mockFetch)
  const query = 'Lost'

  return fetchShows(query).then(result => {
    expect(mockFetch).toHaveBeenCalledWith(
      'http://www.omdbapi.com/?apikey=a9cddb65&type=series&t=' +
        query +
        '&season=1'
    )
    expect(result).toBe('data')
  })
})
