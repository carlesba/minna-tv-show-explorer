const API_KEY = 'a9cddb65'
const ROOT_URL = 'http://www.omdbapi.com/'

const createFetchAPI = fetch => id => {
  const url = ROOT_URL + '?apikey=' + API_KEY + `&i=${id}`
  return fetch(url).then(response => response.json())
}

export default createFetchAPI
