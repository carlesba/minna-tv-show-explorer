const API_KEY = 'a9cddb65'
const ROOT_URL = 'http://www.omdbapi.com/'

const createFetchShows = fetch => query => {
  const url =
    ROOT_URL + '?apikey=' + API_KEY + '&type=series' + `&t=${query}&season=1`
  return fetch(url).then(response => response.json())
}

export default createFetchShows
