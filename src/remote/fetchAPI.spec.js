import createFetchAPI from './fetchAPI'

test('Appends API_KEY to query', () => {
  const mockFetch = jest.fn(() =>
    Promise.resolve({
      json() {
        return 'data'
      }
    })
  )
  const fetchAPI = createFetchAPI(mockFetch)
  const query = '123123'

  return fetchAPI(query).then(result => {
    expect(mockFetch).toHaveBeenCalledWith(
      'http://www.omdbapi.com/?apikey=a9cddb65&i=' + query
    )
    expect(result).toBe('data')
  })
})
