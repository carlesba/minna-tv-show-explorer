const initialState = {
  query: 'Silicon Valley',
  status: 'IDLE'
}

const searchShow = (update, fetchShows) => query => {
  update({ status: 'PENDING' })
  return fetchShows(query)
    .then(data => {
      update({ data, status: 'SUCCESS' })
    })
    .catch(error => {
      update({
        reason: error.message,
        status: 'FAILURE'
      })
    })
}

const Store = (value = initialState, update, fetchShows) => ({
  value,
  updateQuery: query => update({ query }),
  searchShow: searchShow(update, fetchShows)
})

export default Store
