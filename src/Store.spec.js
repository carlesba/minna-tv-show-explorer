import Store from './Store'

test('initialState', () => {
  const store = Store()
  expect(store.value).toEqual({
    query: 'Silicon Valley',
    status: 'IDLE'
  })
})

test('updateQuery updates query value', () => {
  const update = jest.fn()
  const store = Store(undefined, update)
  store.updateQuery('Game of Thrones')
  expect(update).toHaveBeenCalledWith({
    query: 'Game of Thrones'
  })
})

test('searchShow updates app status and data when succeeds', () => {
  const update = jest.fn()
  const fetch = jest.fn(() => Promise.resolve({ id: 1 }))
  const store = Store(undefined, update, fetch)
  const query = 'Game of Thrones'

  return store.searchShow(query).then(() => {
    expect(update).toHaveBeenCalledWith({ status: 'PENDING' })

    expect(fetch).toHaveBeenCalledWith(query)

    expect(update).toHaveBeenNthCalledWith(2, {
      status: 'SUCCESS',
      data: {
        id: 1
      }
    })
  })
})

test('searchShow updates status when something goes wrong', () => {
  const update = jest.fn()
  const fetch = jest.fn(() =>
    Promise.reject({ message: 'something went wrong' })
  )
  const store = Store(undefined, update, fetch)
  const query = 'Game of Thrones'

  return store.searchShow(query).then(() => {
    expect(update).toHaveBeenCalledWith({ status: 'PENDING' })

    expect(fetch).toHaveBeenCalledWith(query)

    expect(update).toHaveBeenNthCalledWith(2, {
      status: 'FAILURE',
      reason: 'something went wrong'
    })
  })
})
